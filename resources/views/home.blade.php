@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <company-table></company-table>
        </div>
    </div>
</div>
@endsection
