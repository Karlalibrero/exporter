# Exporter

This exports companies and employees in csv format

## Getting Started
- install homestead. Follow steps: https://laravel.com/docs/5.7/homestead#introduction
- clone project
- add project to Homestead.yaml and hosts file
- run homestead

- run in project directory
```
composer install

```
- create an .env file and set up database details (use env.example)
- run in project directory
```
php artisan key:generate

php artisan migrate

php artisan db:seed

npm install

npm run dev
```
- create symbolic link for storage:
	- ssh into project
	- run 
	```
	php artisan storage:link
	```

## Authors

* **Karla Librero

## License

This project is licensed under the MIT License


