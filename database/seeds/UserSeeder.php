<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate([
        	'email' => 'admin@admin.com'
        ],[
        	'name' => 'Admin',
        	'password' => bcrypt('password')
        ]);
    }
}
