<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($x = 0; $x < 15; $x++) {
        	$company = factory(App\Company::class)->create();
        	$total = rand(10,25);
			factory(App\Employee::class, $total)->create([
			    'company_id' => $company->id,
			]);
        }
	        
    }
}
