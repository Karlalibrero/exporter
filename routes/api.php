<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function(){
	Route::post('company/pagination', 'CompanyController@pagination');
	Route::post('company/add', 'CompanyController@store');
	Route::post('company/update', 'CompanyController@update');
	Route::post('company/{company}/employee/pagination', 'EmployeeController@pagination');

	Route::post('employee/add', 'EmployeeController@store');
	Route::post('employee/update', 'EmployeeController@update');
	Route::post('employee/csv/export', 'EmployeeController@exportCSV');
	Route::post('employee/csv/download', 'EmployeeController@exportCSV');
});