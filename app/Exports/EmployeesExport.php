<?php

namespace App\Exports;

use App\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithMapping;
class EmployeesExport implements FromCollection, WithMapping
{
    public function collection()
    {
        return Employee::leftJoin('companies', 'employees.company_id', 'companies.id')
            ->select(['employees.first_name','employees.last_name', 'employees.id as employee_id', 
                'companies.name', 'companies.id as company_id', 'employees.created_at'])
            // ->orderBy('companies.name','asc')
            // ->orderBy('employees.last_name','asc')
            ->get();
    }

    public function map($employee): array
    {
        return [
            $employee->last_name.', '.$employee->first_name,
            $employee->employee_id,
            $employee->name,
            $employee->company_id,
            $employee->created_at
        ];
    }
}
