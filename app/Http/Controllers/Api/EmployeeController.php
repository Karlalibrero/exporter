<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Company;
use App\Employee;
use App\Export;
use App\Exports\EmployeesExport;
use Carbon\Carbon;
use Excel;
use Storage;
class EmployeeController extends Controller
{
	public function store(Request $request) {
    	$inputs = $request->all();
    	\Log::info($inputs);

    	$employee = Employee::firstOrCreate([
    		'first_name' => $inputs['employee']['first_name'],
    		'last_name' => $inputs['employee']['last_name'],
    		'company_id' => $inputs['employee']['company_id'],
    	]);

    	return response()->json([
    			'success' => true
    		]);
    }

    public function update(Request $request) {
    	$inputs = $request->all();
    	$employee = Employee::find($inputs['employee']['id']);
    	$employee->first_name = $inputs['employee']['first_name'];
    	$employee->last_name = $inputs['employee']['last_name'];
    	$employee->save();

    	return response()->json([
    			'success' => true
    		]);
    }

    public function exportCSV(Request $request) {
 
    	$create_updated_csv = false;
    	//check if there are any recent updates in companies and employees that is latest than the newest saved csv
    	$latest_export = Export::orderBy('created_at', 'desc')->first();
    	if($latest_export) {
    		$latest_company = Company::orderBy('updated_at', 'desc')->first();
    		if($latest_company) {
    			if($latest_company->updated_at >= $latest_export->created_at) $create_updated_csv = true;
    			if(!$create_updated_csv) {
    				$latest_employee = Employee::orderBy('updated_at', 'desc')->first();
    				if($latest_employee) {
    					if($latest_employee->updated_at >= $latest_export->created_at) $create_updated_csv = true;
    				}
    			}
    		}
    	}else $create_updated_csv = true;

    	if($create_updated_csv) {
    		$export_link = $this->createCSV();
    	}else if(!$create_updated_csv) {
    		$export_link = $latest_export->file_name;
    	}

    	return response()->json([
    		'link' => asset('storage/exports/'.$export_link),
    		'name' => $export_link
    	],200);
    }

    protected function createCSV() {
    	// Store on default disk
    	$date = Carbon::now()->toDateTimeString();
    	$path = 'public/exports/';
    	$file_name = 'employees as of '.$date.'.csv';
    	Excel::store(new EmployeesExport(), $path.$file_name, 'local', \Maatwebsite\Excel\Excel::CSV);

    	$export = new Export;
    	$export->file_name = $file_name;
    	$export->save();

    	return $file_name;
    }

    public function pagination(Request $request, $company) {
    	$filters = $request->all();
    	$employees = Employee::where('company_id', $company)->orderBy($filters['order']['name'], $filters['order']['dir'])
    		->paginate(15);
    	return response()->json($employees);
    }
}
