<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee;
use App\Company;
use DB;

class CompanyController extends Controller
{
    public function pagination(Request $request) {
    	$filters = $request->all();
    	$companies = Company::orderBy($filters['order']['name'], $filters['order']['dir'])
    		->select(DB::RAW('companies.*, (SELECT COUNT(*) FROM employees WHERE company_id = companies.id) as total'))
    		->paginate(10);
    	return response()->json($companies);
    }

    public function store(Request $request) {
    	$inputs = $request->all();
    	$company = Company::firstOrCreate([
    		'name' => $inputs['company']['name']
    	]);

    	return response()->json([
			'success' => true
		]);
    }

    public function update(Request $request) {
    	$inputs = $request->all();
    	$company = Company::find($inputs['company']['id']);
    	$company->name = $inputs['company']['name'];
    	$company->save();

    	return response()->json([
			'success' => true
		]);
    }
}
