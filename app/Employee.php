<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Employee extends Model
{
    protected $fillable = [
    	'first_name',
    	'last_name',
    	'company_id'
    ];

    public function company() {
    	return $this->belongsTo('App\Company', 'company_id');
    }

    public function scopeCompanyTotalEmployees($query, $order_name, $order_dir) {
    	$query->select(['companies.*',DB::RAW('COUNT(*) as total')]);
    	$query->leftJoin('companies', 'employees.company_id', 'companies.id');
    	$query->groupBy('company_id');
    	$query->orderBy($order_name,$order_dir);
    	return $query;
    }
}
